// public/js/app.js
var app = angular.module('GameLibrary', ['ui.bootstrap', 'ui.router', 'ui.chart', 'uiRoutes', 'ngAnimate', 'gamelibServices', 'gamelibDirectives', 'GameCtrl', 'contenteditable', 'ui.calendar']);
app.value('apiServer', '');
app.value('imgServer', '/');

app.filter("emptyToEnd", function () {
    return function (array, key) {
        if (!angular.isArray(array)) return;
        var present = array.filter(function (item) {
            return item[key];
        });
        var empty = array.filter(function (item) {
            return !item[key]
        });
        return present.concat(empty);
    };
});
