// public/js/controllers/GameCtrl.js
var GameCtrl = angular.module('GameCtrl', []);
GameCtrl.controller('GameController', function($scope, $timeout, prefix, GameSrv, gamelibDialog, FilterSrv) {
	//var coverServer;
	//GameSrv.coverServer().success(function(r) { coverServer = JSON.parse(r); });

	$scope.prefix = prefix;

	$scope.games = [];
	$scope.gameDates = [];
	$scope.maxShownGames = 36;
	GameSrv.find().success(function(r) {
		$scope.games = r;
		$scope.gameDates = [r.map(function(game) {
			return {
				title: game.title,
				start: new Date(game.insertion),
				allDay: true
			}
		})];
	});

	$scope.getFilters = function() {
		$scope.filters = FilterSrv.filters;
		$scope.filters.forEach(function(filter) {
			FilterSrv.keys(filter.field).success(function(newKeys) {
				filter.keys = filter.process ? filter.process(newKeys) : newKeys;

				if($scope.activeFilters && $scope.activeFilters[filter.field]) {
					$scope.activeFilters[filter.field] = $scope.activeFilters[filter.field].filter(function(k) {
						return filter.keys.indexOf(k) > -1;
					});
					if($scope.activeFilters[filter.field].length == 0)
						delete $scope.activeFilters[filter.field];
				}
			});
		});
	};
	$scope.getFilters();
	$scope.$on('refreshFilters', function() { $scope.getFilters(); });

	$scope.activeFilters = {  };
	$scope.orderByField = 'insertion';
	$scope.orderReverse = true;

	$scope.in = function(expected, actual) {
		for(var i = 0; i < actual.length; i++) {
			if(expected === actual[i])
				return true;
		}
		return false;
	};

	$scope.createGame = function() {
		console.log('creating game');
		var newGame = { };
		gamelibDialog.show({
			data : newGame,
			newItem : true,
			position : 'center',
			controller : 'GameDetailsController'
		}).then(function () {
			GameSrv.insert(newGame).success(function(r) {
				$scope.games.unshift(r);
				$scope.getFilters();
			});
		});
	};

	$scope.deleteGame = function(game) {
		console.log('deleting ' + game.title);
		window.sweetAlert({
			title: game.title,
			text: 'Borrar?',
			type: 'error',
			showCancelButton: true,
			cancelButtonText: 'Non',
			confirmButtonText: 'Si'
		}, function() {
			$scope.games.splice($scope.games.indexOf(game), 1);
			GameSrv.delete(game);
			$timeout(function() { $scope.getFilters(); }, 1000);
		});
	};

	$scope.moreGames = function() {
		$scope.maxShownGames = Math.min($scope.games.length, $scope.maxShownGames + 12);
	};
});

GameCtrl.controller('ReportController', function($scope, ReportSrv, FilterSrv, GameSrv) {
	$scope.pieChartOptions = {
		seriesDefaults : {
			renderer : jQuery.jqplot.PieRenderer,
			rendererOptions : {
				dataLabels: 'value',
				dataLabelThreshold: 0,
				dataLabelPositionFactor: 0.8,
				showDataLabels : true,
				diameter : 300,
				padding : 0,
				shadow : false,
			}
		},
		highlighter : {
			show : true,
			sizeAdjust : 2,
			formatString : '%s - %p',
			tooltipAxes: 'xy',
			tooltipOffset : 2,
			showTooltip : true,
			tooltipLocation : 'ne',
			useAxesFormatters : false
		},
		legend : {
			show : true,
			location : 'se',
			fontSize : 'x-small',
			rowSpacing : '0.7m',
			fontFamily: 'Courier New',
			rendererOptions : {
				numberColumns : 1
			}
		},
		grid : {
			background : 'transparent',
			drawBorder : false,
			shadow : false
		}
	};

	$scope.pieChartOptionsSmall = {
		seriesDefaults : {
			renderer : jQuery.jqplot.PieRenderer,
			rendererOptions : {
				dataLabels: 'value',
				dataLabelThreshold: 0,
				dataLabelPositionFactor: 0.8,
				showDataLabels : true,
				diameter : 150,
				padding : 0,
				shadow : false,
			}
		},
		highlighter : {
			show : true,
			sizeAdjust : 2,
			formatString : '%s - %p',
			tooltipAxes: 'xy',
			tooltipOffset : 2,
			showTooltip : true,
			tooltipLocation : 'ne',
			useAxesFormatters : false
		},
		legend : {
			show : true,
			location : 'ne',
			fontSize : 'x-small',
			rowSpacing : '0.7m',
			fontFamily: 'Courier New',
			rendererOptions : {
				numberColumns : 1
			}
		},
		grid : {
			background : 'transparent',
			drawBorder : false,
			shadow : false
		}
	};

	$scope.yearChartOptions = {
		seriesDefaults : {
			renderer : jQuery.jqplot.BarRenderer,
			rendererOptions : {
				shadow : false
			}
		},
		grid : {
			background : 'transparent',
			drawBorder : false,
			shadow : false
		},
		axes : {
			xaxis : {
				renderer : jQuery.jqplot.CategoryAxisRenderer
			}
		},
		highlighter : {
			show : true,
			sizeAdjust : 2,
			formatString : '%d',
			tooltipOffset : 2,
			showTooltip : true,
			tooltipLocation : 'ne',
			useAxesFormatters : false,
			tooltipAxes: 'y',
			tooltipContentEditor: function (str, seriesIndex, pointIndex, plot) {
				return plot.data[seriesIndex][pointIndex];
			}
		},
	};

	ReportSrv.bySystem().success(function(r) {
		r[0] = r[0].sort(function(a, b) { return b[1] - a[1]; });
		$scope.bySystem = r;
	});
	ReportSrv.byGenre().success(function(r) {
		r[0] = r[0].sort(function(a, b) { return b[1] - a[1]; });
		$scope.byGenre = r;
	});
	ReportSrv.byYear().success(function(r) {
		$scope.byYear = [ r[0].map(function(c) { return c[1]; }) ];
		$scope.yearChartOptions.axes.xaxis.ticks = r[0].map(function(c) { return c[0]; });
	});
	ReportSrv.topDevs().success(function(r) {
		$scope.top10devs = r;
	});
	ReportSrv.topPubs().success(function(r) {
		$scope.top10pubs = r;
	});
	ReportSrv.topGames().success(function(r) {
		$scope.top10games = r;
	});

	GameSrv.count().success(function(c) { $scope.numGames['total'] = c; });
	$scope.detailsBySystem = [];
	$scope.detailsByGenre = [];
	$scope.numGames = {};
	FilterSrv.keys('system').success(function(r) {
		$scope.detailsBySystem = r.map(function(k) { return { name: k }; });
		$scope.detailsBySystem.forEach(function(s) {
			GameSrv.count('system', s.name).success(function(c) {
				$scope.numGames[s.name] = c;
			});
			ReportSrv.genresBySystem(s.name).success(function(r) {
				r[0] = r[0].sort(function(a, b) { return b[1] - a[1]; });
				s.genres = r;
			});
			ReportSrv.devsBySystem(s.name).success(function(r) {
				s.devs = r;
			});
			ReportSrv.gamesBySystem(s.name).success(function(r) {
				s.games = r;
			});
		});
	});

	FilterSrv.keys('genre').success(function(r) {
		$scope.detailsByGenre = r.map(function(k) { return { name: k }; });
		$scope.detailsByGenre.forEach(function(g) {
			GameSrv.count('genre', g.name).success(function(c) {
				$scope.numGames[g.name] = c;
			});
			ReportSrv.systemsByGenre(g.name).success(function(r) {
				r[0] = r[0].sort(function(a, b) { return b[1] - a[1]; });
				g.systems = r;
			});
			ReportSrv.devsByGenre(g.name).success(function(r) {
				g.devs = r;
			});
			ReportSrv.gamesByGenre(g.name).success(function(r) {
				g.games = r;
			});
		});
	});
});

GameCtrl.controller('CoverSearchController', function ($http, $scope, $modalInstance, CoverSrv, title, system) {
	CoverSrv.find(title, system).success(function(r) { console.log(r); $scope.covers = r.items });
	$scope.title = title;
	$scope.system = system;
	$scope.coverdlg = { other : "" };

	$scope.ok = function(cover) {
		console.log('selected cover ' + cover);
		$modalInstance.close(cover);
	};

	$scope.customOk = function() {
		console.log('customUrl: ' + $scope.coverdlg.other);
		var cover = {
			link: $scope.coverdlg.other,
			image: {
				thumnailLink: $scope.coverdlg.other
			}
		};
		$scope.ok(cover);
	};
});

GameCtrl.controller('GameDetailsController', function($scope, $rootScope, $modal, $timeout, prefix, GameSrv) {
	//var coverServer;
	//GameSrv.coverServer().success(function(r) { coverServer = JSON.parse(r); });

	$scope.prefix = prefix;

	$scope.setEditMode = function() {
		$scope.original = JSON.stringify($scope.details);
		$scope.editMode = true;
		$scope.$broadcast('startEdit');
		delete $scope.invalid;
	};

	if($scope.newItem) {
		$scope.editMode = true;
		$scope.$broadcast('startEdit');

		$scope.dismissEdit = function() {
			$scope.$broadcast('endEdit');
			$scope.close(false);
		};

		$scope.acceptEdit = function() {
			$scope.invalid = !$scope.validate();
			if(!$scope.invalid) {
				$scope.$broadcast('endEdit');
				if(!$scope.details.digital)
					$scope.details.digital = false;
				$scope.close(true);
			}
		};
	}
	else if(!$scope.newItem) {
		$scope.editMode = false;
		$scope.dismissEdit = function() {
			$scope.$broadcast('endEdit');
			if($scope.original)
				$scope.details = JSON.parse($scope.original);
			$scope.editMode = false;
		};

		$scope.acceptEdit = function() {
			$scope.invalid = !$scope.validate();
			$scope.editMode = $scope.invalid;
			if(!$scope.details.comments) $scope.details.comments = " ";
			if(!$scope.invalid) {
				$scope.$broadcast('endEdit');
				if(!$scope.details.digital)
					$scope.details.digital = false;
				GameSrv.update($scope.details);
				$timeout(function() { console.log('updated'); $rootScope.$broadcast('refreshFilters'); }, 1000);
			}
		};

		$scope.updateRating = function() {
			GameSrv.changeRating($scope.details._id, $scope.details.rating);
			$timeout(function() { $rootScope.$broadcast('refreshFilters'); }, 1000);
		};
	}

	$scope.searchCover = function() {
		if($scope.editMode) {
			$modal.open({
				templateUrl: 'views/cover-search-dialog.html',
				controller: 'CoverSearchController',
				size: 'lg',
				resolve : {
					title : function() { return $scope.details.title; },
					system : function() { return $scope.details.system; },
				},
			}).result.then(function(cover) {
				$scope.details.coverArt = cover.link;
				$scope.details.coverArtThumb = cover.image.thumbnailLink;
			});
		}
	};
});
