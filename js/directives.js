var gamelibDirectives = angular.module('gamelibDirectives', []);
gamelibDirectives.directive('game', function($controller, gamelibDialog) {
	return {
		restrict: 'E',
		scope : { details: '=' },
		link: function(scope, el, attrs) {
			$(el).on('click', function(e) {
				console.log('clicked: ' + e.clientX + ',' + e.clientY);
				gamelibDialog.show({
					top : e.clientY,
					left : e.clientX,
					newItem : false,
					data : scope.details,
					controller : 'GameDetailsController'
				});
				scope.$apply();
			});
		}
	};
});

gamelibDirectives.directive('hint', function() {
	return {
		restrict: 'A',
		link: function(scope, el, attrs) {
			scope.$watch(attrs.ngModel, function(newVal, oldVal) {
				newVal && newVal != " " ? $(el).removeClass('input-empty') : $(el).addClass('input-empty');
			});
		}
	};
});

gamelibDirectives.directive('number', function() {
	return {
		restrict: 'A',
		link: function(scope, el, attrs) {
			scope.$watch(attrs.ngModel, function(newVal, oldVal) {
				/^[0-9]+$/.test(newVal) ? $(el).removeClass('ng-invalid').removeClass('nan').addClass('ng-valid') :
														 $(el).removeClass('ng-valid').addClass('ng-invalid').addClass('nan');
			});
		}
	};
});

gamelibDirectives.directive('errors', function($compile) {
	return {
		restrict: 'A',
		compile: function(el, attr) {
			el.attr('class', 'errors');
			el.removeAttr('errors');
			el.attr('ng-if', 'editMode && invalid');
			var fn = $compile(el);
			return function(scope) { fn(scope); };
		},
		template: 'campos erróneos'
	};
});

gamelibDirectives.directive('gameDetailsDialog', function($timeout) {
	return {
		restrict: 'A',
		templateUrl: function(el, attrs) {
			return attrs.templateUrl;
		},
		compile: function(el, attrs) {
			$(el).find('[contenteditable]').attr('contenteditable', '{{ editMode }}');
			return {
				post: function(scope, el, attrs) {
					window.history.pushState('dialog', 'dialog');
					$(window).bind('popstate', function() { scope.close(false); });
					$('body').after($(el));
					$timeout(function() {
						attrs.left = parseInt(attrs.left);
						$(el).find('#rating').rating();

						var popWidth = $('.game-details-dialog').width();
						if(jQuery.mobile) {
							var left = '5px';
							popWidth = (window.screen.width - 10) + 'px';
						}
						if(attrs.position && attrs.position=='center') {
							var left = 'calc(100vw / 2 - ' + parseInt(popWidth/2) + 'px)';
						} else {
							var offX = $(window).width() - (attrs.left + popWidth + 50);
							var left = offX < 0 ? attrs.left + offX : attrs.left;
						}

						// Adjusting dialog height:
						// First, position the container on its place
						$(el).find('#rating').rating('update', scope.details.rating);
						$(el).css({
							left: left,
							visibility: 'visible'
						});

						$('[tabindex=1]').focus();

						$('body').after('<div id="back"></div>');
						$('#back').on('click', function() {
							if(!scope.editMode) {
								$(el).remove();
								$(this).remove();
							}
						});
					});

					scope.$on('startEdit', function() {
						$(el).find('[tabindex=1]').focus();
					});

					$(el).find('span[contenteditable]').keydown(function(e) {
						return e.which != 13; 
					});
					$(el).on('rating.change', function(event, value) {
						scope.details.rating = parseFloat(value);
						scope.updateRating(scope.details._id, scope.details);
						scope.$apply();
					});

					scope.close = function(accepted) {
						$(el).remove();
						$('#back').remove();
						if(accepted) {
							scope.result.resolve(true);
						}	else {
							scope.result.reject(false);
						}
					};

					scope.validate = function(accepted) {
						if(!$(el).find('.ng-invalid:not(.rating)').length) {
							return true;
						} else {
							$(el).find('.ng-pristine').addClass('ng-dirty').removeClass('ng-pristine');
							return false;
						}
					};
				}
			};
		}
	};
});

gamelibDirectives.directive('treeBranch', function($animate) {
	return {
		restrict: 'A',
		transclude: true,
		link: function(scope, el, attrs) {
			$(el).find('label').on('click', function() {
				$(el).find('.filter-keys').each(function(i,v) {
					if($(v).hasClass('collapsed'))
						$animate.removeClass(v, 'collapsed');
					else
						$animate.addClass(v, 'collapsed');
				});
				scope.$apply();
			});
		},
		template: '<div class="tree-branch" ng-transclude></div>'
	};
});

gamelibDirectives.directive('treeOption', function($state) {
	return {
		restrict: 'A',
		transclude: true,
		template: '<span ng-transclude></span>',
		link: function(scope, el, attrs) {
			$(el).on('click', function() {
				if(!scope.$parent.activeFilters) scope.$parent.activeFilters = { };
				var activeFilters = scope.$parent.activeFilters;
				if(!activeFilters[attrs.treeOption]) activeFilters[attrs.treeOption] = [ ];
				var filters = activeFilters[attrs.treeOption];
				var key = scope[attrs.key];

				if($(el).hasClass('selected')) {
					var i = filters.indexOf(key);
					filters.splice(i, 1);
					if(filters.length == 0)
						delete activeFilters[attrs.treeOption];
					$(el).removeClass('selected');
				} else {
					filters.push(key);
					$(el).addClass('selected');
				}
				scope.$apply()
				if($state.is('games.reports'))
					$state.transitionTo('games.bookshelf');
			});
		}
	};
});

gamelibDirectives.directive('ckEditor', function($timeout, GameSrv) {
	return {
		restrict: 'AE',
		link: function(scope, el, attrs) {
			if(scope.newItem) {
				$timeout(function() { CKEDITOR.inline('editor') }, 600);
			}
			scope.$on('startEdit', function() {
				CKEDITOR.inline('editor');
			});
			scope.$on('endEdit', function() {
				if(CKEDITOR.instances.editor)
					CKEDITOR.instances.editor.destroy();
			});
		}
	}
});

gamelibDirectives.directive('tableEdit', function(GameSrv, $rootScope, $timeout) {
	return {
		restrict: 'A',
		link: function(scope, el, attrs) {
			$(el).keydown(function(e) {
				var ti = parseInt(attrs.tabindex);
				if(e.which == 13 || e.which == 40) {
					$('[tabindex='+(ti+8)+']').focus();
					return false;
				} else if(e.which == 38 && ti > 8) {
					$('[tabindex='+(ti-8)+']').focus();
					return false;
				} else if(e.which != 9 && e.which != 16) {
					$(el).addClass('ng-dirty');
				}
			});
			$(el).on('blur', function() {
				if($(el).hasClass('ng-dirty')) {
					GameSrv.update(scope.game);
					$timeout(function() { $rootScope.$broadcast('refreshFilters'); }, 1000);
				}
			});
			if($(el).attr('type')=="checkbox") {
				$(el).on('change', function() {
					GameSrv.update(scope.game);
				});
			}
		}
	};
});

gamelibDirectives.directive('infiniteScroll', function() {
	return {
		restrict: 'A',
		link: function($scope, element, attrs) {
			element.on('scroll', function() {
				if(element[0].offsetHeight + element[0].scrollTop >= element[0].scrollHeight) {
					$scope.$apply(attrs.infiniteScroll);
				}
			});
		}
	};
});

