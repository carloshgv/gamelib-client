// public/js/appRoutes.js
/*angular.module('appRoutes', []).config(function($routeProvider, $locationProvider) {

$routeProvider
	// home page
	.when('/games', {
		templateUrl: '/games/views/games.html',
		controller: 'GameController'
	})

$locationProvider.html5Mode(true);
});*/

angular.module('uiRoutes', []).config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	$locationProvider.html5Mode(false);
	$urlRouterProvider.otherwise('/');
	$stateProvider
	.state('games', {
		abstract: true,
		templateUrl: 'views/games.html',
		controller: 'GameController'
	})
	.state('games.bookshelf', {
		url: "/",
		templateUrl: 'views/bookshelf-view.html'
	})
	.state('games.table', {
		url: '/table',
		templateUrl: 'views/table-view.html'
	})
	.state('games.reports', {
		url: '/reports',
		templateUrl: 'views/report-view.html',
		controller: 'ReportController'
	});
});
