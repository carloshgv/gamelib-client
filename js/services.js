gamelibServices = angular.module('gamelibServices', []);
gamelibServices.factory('gamelibDialog', function($q, $rootScope, $compile, $templateCache, $timeout) {
	return {
		show : function(config) {
			var localScope = $rootScope.$new();
			localScope.details = config.data;
			localScope.editMode = config.editMode;
			localScope.newItem = config.newItem;

			var dialogContainer = angular.element('<div game-details-dialog class="game-details-dialog"></div>');
			dialogContainer.attr({
				'position': config.position,
				'top': config.top,
				'left': config.left,
				'ng-controller' : config.controller,
				'template-url' : 'views/game-details-dialog.html'
			});
			$compile(dialogContainer)(localScope);
			localScope.result = $q.defer();
			return localScope.result.promise;
		}
	};
});

gamelibServices.factory('GameSrv', function($http, apiServer) {
	return {
		server : apiServer,
		find : function (game) {
			if(!game) {
				return $http.get(this.server + '/api/games');
			}
		},

		update : function(game) {
			return $http.put(this.server + '/api/games/' + game._id, game);
		},

		delete : function(game) {
			return $http.delete(this.server + '/api/games/' + game._id);
		},

		changeRating : function(id, rating) {
			return $http.put(this.server + '/api/games/' + id, { rating: rating	});
		},

		insert : function(game) {
			return $http.post(this.server + '/api/games', game);
		},

		coverServer : function() {
			return $http.get(this.server + '/api/coverServer');
		},
		
		count : function(field, key) {
			if(field)
				return $http.get(this.server + '/api/games/count/' + field + '/' + key);
			else
				return $http.get(this.server + '/api/games/count');
		}
	}
});

gamelibServices.factory('ReportSrv', function($http, apiServer) {
	return {
		server: apiServer,
		bySystem : function() {
			return $http.get(this.server + '/api/reports/by-system');
		},
		byGenre : function() {
			return $http.get(this.server + '/api/reports/by-genre');
		},
		byYear : function() {
			return $http.get(this.server + '/api/reports/by-year');
		},
		topDevs : function() {
			return $http.get(this.server + '/api/reports/top-devs');
		},
		topPubs : function() {
			return $http.get(this.server + '/api/reports/top-pubs');
		},
		topGames : function() {
			return $http.get(this.server + '/api/reports/top-rated');
		},
		genresBySystem : function(s) {
			return $http.get(this.server + '/api/reports/system/' + s + '/genres');
		},
		devsBySystem : function(s) {
			return $http.get(this.server + '/api/reports/top-devs/system/' + s);
		},
		gamesBySystem : function(s) {
			return $http.get(this.server + '/api/reports/top-rated/system/' + s);
		},
		systemsByGenre : function(g) {
			return $http.get(this.server + '/api/reports/genre/' + g + '/systems');
		},
		devsByGenre : function(g) {
			return $http.get(this.server + '/api/reports/top-devs/genre/' + g);
		},
		gamesByGenre : function(g) {
			return $http.get(this.server + '/api/reports/top-rated/genre/' + g);
		}
	};
});

gamelibServices.factory('CoverSrv', function($http) {
	return {
		find : function(title, system) {
			var query = {
				q: title + ' ' + system,
				searchType: 'image',
				fields: 'items(image/thumbnailLink,link)',
				num: 10,
				cx: '015446186962874116375:zuiins-k9s4',
				key: 'AIzaSyDIns-19YU-9vJOKRLOXAb0CCXj6iRlql4'
			};
			return $http.get('https://www.googleapis.com/customsearch/v1', { params: query });
		}
	}
});

gamelibServices.factory('prefix', function(imgServer) {
	return function(path, def) {
		if(!path)
			return def;
		if(path.slice(0, 4) == "http")
			return path;
		return imgServer + path;
	};
});

gamelibServices.factory('FilterSrv', function($http, apiServer) {
	return {
		filters : [ 
			{
				name: "Sistema",
				field: "system",
				label: function(k) {}
			},
			{
				name: "Xénero",
				field: "genre",
				label: function(k) {}
			},
			{	
				name: "Formato",
				field: "digital",
				label: function(k) {
					if(k===false) return "Físico";
					else if(k===true) return "Dixital";
					else return k;
				}
			},
			{
				name: "Desenvolvedora",
				field: "developer",
				label: function(k) {}
			},
			{
				name: "Editora",
				field: "publisher",
				label: function(k) {}
			},
			{
				name: "Ano",
				field: "year",
				label: function(k) {}
			},
			{
				name: "Rating",
				field: "rating",
				process: function(keys) {
					return keys.sort(function(a, b) { return a-b; });
				},
				label: function(k) {
					var label = new Array(Math.floor(k)+1).join('\u2605');
					if(Math.floor(k) != k)
						label += '\u2606';
					return label;
				}
			}
		],
		keys : function(f) {
			return $http.get(apiServer + '/api/distinct/' + f);
		}
	};
});
